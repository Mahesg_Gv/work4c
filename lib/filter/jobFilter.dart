import 'package:flutter/material.dart';
import 'package:work4c/filter/workIn.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart'as frs;
import 'package:work4c/sidebar/sidebar.dart';


import 'jobDetails.dart';
import 'list.dart';


class jobFilter extends StatefulWidget {
  @override
  _jobFilterState createState() => _jobFilterState();
}

class _jobFilterState extends State<jobFilter> {


  double _lowerValue = 10.0;
  double _upperValue = 20.0;
  bool c1 =false;
  bool c2 =true;
  bool c3 =false;
  bool isSwitched = true;
  bool isSwitched1 = true;
  bool isSwitched2 = true;
  bool isChecked = false;
  bool isChecked1 = false;
  String dropdownValue = 'San fransico';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF3F0361),
        centerTitle: true,
        title: Text("Sort and Filter",style: new TextStyle(
            fontSize: 16.0,
            fontFamily: 'Montserrat',
            color: Colors.white ,fontWeight: FontWeight.normal
        ),),
        actions: <Widget>[
      IconButton(icon: new Icon(Icons.close),
      onPressed: (){
        Navigator.pop(context);
        // _showMaterialSearch(context);
      },
    ),
        ],
      ),
      drawer: MyDrawer(),

body: ListView(
        children: <Widget>[
          Container(
              height: 40,color: Color(0xFFE8E8E8),
              child: Padding(
                padding: const EdgeInsets.only(left:25.0,right: 25),
                child: Center(child: Row(
                  children: <Widget>[
                    Text("Sort By",
                      style: new TextStyle(fontSize: 15.0,fontWeight: FontWeight.w500),

                    ),
                  ],
                )),
              )),
            Padding(
              padding: const EdgeInsets.only(left:29.0,right: 29,top: 30,bottom: 30),
              child: Container(
                height: 45,
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                    border: new Border.all(color: Color(0xFFEB5757))
                ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      setState(() {
                        c1 =true;
                        c2=false;
                        c3=false;
                      });
                    },
                    child: Container(
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.only(bottomLeft: const Radius.circular(5.0),topLeft: const Radius.circular(5.0) ),
                            border: new Border.all(color: Colors.transparent),
                          color:c1 ? Colors.red : Colors.white,
                        ),
                        width: MediaQuery.of(context).size.width/ 3 -60/3  ,

                        child: Center(child: Text("Start Date",style: new TextStyle(fontSize: 14.0,color: c1 ?Colors.white: Colors.black),))
                    ),
                  ),

                  GestureDetector(
                    onTap: (){
                      setState(() {
                        c2 =true;
                        c1 =false;
                        c3=false;
                      });
                    },
                    child: Container(
                        width: MediaQuery.of(context).size.width/ 3 -60/3  ,
                      color:c2 ? Colors.red : Colors.white,
                        child: Center(child: Text("Hourly Rate",style: new TextStyle(fontSize: 14.0,color: c2 ?Colors.white: Colors.black),))
                    ),
                  ),

                  GestureDetector(
                    onTap: (){
                      setState(() {
                        c3 =true;
                        c1 =false;
                        c2=false;
                      });
                    },
                    child: Container(
                        width: MediaQuery.of(context).size.width/ 3 -60/3  ,
                        decoration: new BoxDecoration(
                          borderRadius: BorderRadius.only(bottomRight: const Radius.circular(5.0),topRight: const Radius.circular(5.0) ),
                          border: new Border.all(color: Colors.transparent),
                          color:c3 ? Colors.red : Colors.white,
                        ),
                        child: Center(child: Text("Open Slots",style: new TextStyle(fontSize: 14.0,color: c3 ?Colors.white: Colors.black))
                    ),
                  ),

                  )
                ],
              ),
              ),
            ),

          Container(
              height: 41,color: Color(0xFFE8E8E8),
              child: Padding(
                padding: const EdgeInsets.only(left:25.0,right: 25),
                child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Filter",style: new TextStyle( fontSize: 15,fontWeight: FontWeight.w500
                    ),),

                    GestureDetector(
                      onTap: (){
                        setState(() {
                           _lowerValue = 10.0;
                           _upperValue = 20.0;
                           c1 =true;
                           c2 =false;
                           c3 =false;
                           isSwitched = true;
                           isSwitched1 = true;
                           isSwitched2 = true;
                           isChecked = false;
                           isChecked1 = false;
                           dropdownValue = 'San fransico';
                        });
                      },
                      child: Text("Clear All",style: new TextStyle(fontSize: 14.0,
                          color: Colors.red
                      ),),
                    ),
                  ],
                )),
              )),


            Container(height: 20,),
          Padding(
            padding: const EdgeInsets.only(left:0,right: 0),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left:25.0,right: 25),
                  child: Row(
                    children: <Widget>[
                      Text("Hourly Rates",style: new TextStyle(fontSize: 14.0,)),
                    ],
                  ),
                ),
                Container(height: 20,),
                Padding(
                  padding: const EdgeInsets.only(left:25.0,right: 25),
                  child: new frs.RangeSlider(
                    min: 0.0,
                    max: 100.0,
                    lowerValue: _lowerValue,
                    upperValue: _upperValue,
                    divisions: 100,
                    showValueIndicator: true,
                    valueIndicatorMaxDecimals:1,
                    onChanged: (double newLowerValue, double newUpperValue) {
                      setState(() {
                        _lowerValue = newLowerValue;
                        _upperValue = newUpperValue;


                      });
                    },

                    onChangeStart:
                        (double startLowerValue, double startUpperValue) {
                      print(
                          'Started with values: $startLowerValue and $startUpperValue');
                    },
                    onChangeEnd: (double newLowerValue, double newUpperValue) {
                      print(
                          'Ended with values: $newLowerValue and $newUpperValue');
                    },
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 3),
                  child: Container(height: 1,color: Color(0xFFBCBCBC),),
                ),

                Padding(
                  padding: const EdgeInsets.only(left:25.0,right: 25),
                  child: Row(
                    children: <Widget>[
                      Text("Odd Shifts",style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black
                      ),),
                      Container(width: 50,),
                      Switch(
                        value: isSwitched,
                        onChanged: (value) {
                          setState(() {
                            isSwitched = value;
                          });
                        },
                        activeTrackColor: Color(0xFFEB5757),
                        activeColor: Colors.red,
                      ),
                    ],
                  ),
                ),

            Padding(
              padding: const EdgeInsets.only(left:25.0,right: 25),
              child: Row(
                children: <Widget>[
                  Container(width: 100,),
                  Checkbox(
                  value: isChecked,
                  checkColor: Colors.white,
                  activeColor: Colors.red,
                  onChanged: (value) {
                    setState(() {
                      isChecked = value;
                    });
                  },
                  ),
                  Text("Weekend",style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.black
                  ),),

                  Checkbox(
                  value: isChecked1,
                    activeColor: Colors.red,
                  onChanged: (value) {
                    setState(() {
                      isChecked1 = value;
                    });
                  },
                  ),
                  Text("Night Shift",style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.black
                  ),),

                ],
              ),
            ),

                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 3),
                  child: Container(height: 1,color: Color(0xFFBCBCBC),),
                ),

                Padding(
                  padding:  const EdgeInsets.only(left:25.0,right: 25),
                  child: Row(
                    children: <Widget>[
                      Text("Over Time",style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black
                      ),),
                      Container(width: 40,),
                      Switch(
                        value: isSwitched1,
                        onChanged: (value) {
                          setState(() {
                            isSwitched1 = value;
                          });
                        },
                        activeTrackColor: Color(0xFFEB5757),
                        activeColor: Colors.red,
                      ),
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 3),
                  child: Container(height: 1,color: Color(0xFFBCBCBC),),
                ),

                Padding(
                  padding: const EdgeInsets.only(left:25.0,right: 25),
                  child: Row(
                    children: <Widget>[
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("Only Companies ",style: new TextStyle(
                                fontSize: 14.0,
                                color: Colors.black
                            ),),
                            Text("I Follow",style: new TextStyle(
                                fontSize: 14.0,
                                color: Colors.black
                            ),),
                          ],
                        ),
                      ),
                      Switch(
                        value: isSwitched2,
                        onChanged: (value) {
                          setState(() {
                            isSwitched2 = value;
                          });
                        },
                        activeTrackColor: Color(0xFFEB5757),
                        activeColor: Colors.red,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: Container(height: 1,color: Color(0xFFBCBCBC),),
                ),
                Padding(
                  padding:  const EdgeInsets.only(left:25.0,right: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width*0.65,
                        height: 80,
                        color: Color(0xFFEB5757),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("loc"),
                        ),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Locations",style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.black
                          ),),
                          GestureDetector(
                            onTap: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (BuildContext context) => BookFinderPage(text:"Locations"))
                              );
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width*0.65,
                              height: 30,
                              color: Colors.grey,
                              child:  Row(
                                children: <Widget>[
                                  Icon(Icons.arrow_drop_down,size: 25,),
                                  Padding(
                                    padding: const EdgeInsets.only(left:25.0),
                                    child: Text("[ select ]"),
                                  ),
                                ],
                              )
//                            Padding(
//                              padding: const EdgeInsets.only( left:80.0),
//                              child: DropdownButton<String>(
//                                value: dropdownValue,
//                                onChanged: (String newValue) {
//                                  setState(() {
//                                    dropdownValue = newValue;
//                                  });
//                                },
//                                items: <String>['Pune','Bangalore', 'San fransico', 'Delhi']
//                                    .map<DropdownMenuItem<String>>((String value) {
//                                  return DropdownMenuItem<String>(
//                                    value: value,
//                                    child: Text(value),
//                                  );
//                                })
//                                    .toList(),
//                              ),
//                            ),
                            ),
                          ),
                        ],
                      ),

                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Container(height: 1,color: Color(0xFFBCBCBC),),
                ),

                Padding(
                  padding:  const EdgeInsets.only(left:25.0,right: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width*0.65,
                        height: 80,
                        color: Color(0xFFEB5757),
                        child: Padding(
                          padding: const EdgeInsets.all( 8),
                          child: Text("Carpenter, Electrician, Construction, Waiter, Security"),
                        ),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Type of work",style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.black
                          ),),
                          GestureDetector(
                            onTap: (){
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (BuildContext context) => BookFinderPage(text:"Type of work"))
                              );
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width*0.65,
                              height: 30,
                              color: Colors.grey,
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.arrow_drop_down,size: 25,),
                                  Padding(
                                    padding: const EdgeInsets.only(left:25.0),
                                    child: Text("[ select ]"),
                                  ),
                                ],
                              )
//                            Padding(
//                              padding: const EdgeInsets.only( left:80.0),
//                              child: DropdownButton<String>(
//                                value: dropdownValue,
//                                onChanged: (String newValue) {
//                                  setState(() {
//                                    dropdownValue = newValue;
//                                  });
//                                },
//                                items: <String>['Pune', 'Bangalore', 'San fransico', 'Delhi']
//                                    .map<DropdownMenuItem<String>>((String value) {
//                                  return DropdownMenuItem<String>(
//                                    value: value,
//                                    child: Text(value),
//                                  );
//                                })
//                                    .toList(),
//                              ),
//                            ),
                            ),
                          ),
                        ],
                      ),

                    ],
                  ),
                ),





              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(left:65.0,right: 65,top: 40),
            child: GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext   context) => new jobdetails(),
                  ),
                );
              },
              child: Container(
                height: 42,
                decoration: new BoxDecoration(
                  //shape:BoxShape.circle ,
//                  borderRadius: BorderRadius.circular(30.0),
                  gradient: const LinearGradient(
                    begin: FractionalOffset.centerRight,
                    end: FractionalOffset.centerLeft,
                    colors: <Color>[
                      const Color(0xFFFF473A),
                      const Color(0xFFFF473A),
                    ],
                  ),
                ),
                child: Center(
                  child: Text("Apply Filters",style: TextStyle(
                      fontSize: 14 ,fontWeight: FontWeight.bold,
                      color: Color(0xFFFFFFFF))),
                ),


              ),
            ),
          ),
          Container(height: 30,),

        ],
      ),
    );
  }
}
