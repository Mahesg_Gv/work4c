import 'package:flutter/material.dart';

import 'cmpDetails.dart';
import 'jobDetails.dart';
class cmpjobcard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 130,
      child: GestureDetector(
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext   context) => new jobdetails(),
            ),
          );

        },
        child: Card(
          child: Row(
            children: <Widget>[
              Container(
                  child: Padding(
                    padding: const EdgeInsets.only(left:20,right: 20,top: 8,bottom: 8),
                    child: Column(
                      children: <Widget>[
                        Text("Hourly rate",style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),),
                        Padding(
                          padding: const EdgeInsets.only(top:6.0,bottom: 6),
                          child: Text("\$10 / \$14",style: new TextStyle(
                              fontSize: 18.0,
                              color: Colors.black ,fontWeight: FontWeight.bold
                          ),),
                        ),
                        Text("13 july 2019",style: new TextStyle(
                            fontSize: 14.0,
                            color: Colors.black
                        ),),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 2),
                          child: Text("To",style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.black
                          ),),
                        ),
                        Text("23 july 2019",style: new TextStyle(
                            fontSize: 14.0,
                            color: Colors.black
                        ),),

                      ],
                    ),
                  )),
              Padding(
                padding: const EdgeInsets.only(left:8.0),
                child: Container(
                  height: 100,width: 1,color: Colors.blue,
                ),
              ),

              Container(
                  width: MediaQuery.of(context).size.width -161,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        Text("Work in site - Construction",style: new TextStyle(
                            fontSize: 14.0,
                            color: Colors.black,fontWeight: FontWeight.bold
                        ),),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 6),
                          child: Text("CSGNY IT Consuluting Pvt Ltd.",style: new TextStyle(
                              fontSize: 11.0,
                              color: Colors.black
                          ),),
                        ),
                      Container(height: 20,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text("Required",style: new TextStyle(
                                fontSize: 14.0,
                                color: Colors.black ,fontWeight: FontWeight.normal
                            ),),
//                            Text("Appleid",style: new TextStyle(
//                                fontSize: 14.0,
//                                color: Colors.black ,fontWeight: FontWeight.normal
//                            ),),
                          ],
                        ),
                        Padding(padding: EdgeInsetsDirectional.only(bottom: 2)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text("10",style: new TextStyle(
                                fontSize: 14.0,
                                color: Colors.black ,fontWeight: FontWeight.bold
                            ),),
//                            Text("20",style: new TextStyle(
//                                fontSize: 14.0,
//                                color: Colors.black ,fontWeight: FontWeight.bold
//                            ),),
                          ],
                        ),


                      ],
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
