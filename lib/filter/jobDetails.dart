import 'package:flutter/material.dart';
import 'package:work4c/home/notifnIcon.dart';
import 'package:work4c/home/thanks.dart';
import 'package:work4c/sidebar/sidebar.dart';

class jobdetails extends StatefulWidget {
  @override
  _jobdetailsState createState() => _jobdetailsState();
}

class _jobdetailsState extends State<jobdetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF3F0361), //No more green
        elevation: 0.0,
          actions: <Widget>[
            ntfnIcon(),
          ],
      ),
      drawer: MyDrawer(),
      body: Container(
          height: MediaQuery.of(context).size.height,
          decoration: new BoxDecoration(
            //shape:BoxShape.circle ,
//                  borderRadius: BorderRadius.circular(30.0),
            gradient: const LinearGradient(
              begin: FractionalOffset.topCenter,
              end: FractionalOffset.bottomCenter,
              colors: <Color>[
                const Color(0xFF3F0361),
                const Color(0xFF711D57),
              ],
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(left:26.0,right: 26),
            child: SingleChildScrollView(

              child: Column(
                children: <Widget>[
                  Container(
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                        border: new Border.all(color: Colors.white)
                    ),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left:22.0,right: 2,top: 8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text("Work in site - Construction",style: TextStyle(
                                            fontSize: 16 ,fontWeight: FontWeight.bold,
                                            color: Colors.black)),

                                        Padding(
                                          padding: const EdgeInsets.only(top:5.0),
                                          child: Text("CSGNY IT Consuluting Pvt Ltd.",style: TextStyle(
                                              fontSize: 11 ,fontWeight: FontWeight.normal,
                                              color: Colors.black)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Column(
                                    children: <Widget>[
                                      IconButton(icon: Icon(Icons.close,), onPressed: (){
                                        Navigator.pop(context);
                                      }),
                                    ],
                                  ),
                                ],
                              ),
//                              Padding(
//                                padding: const EdgeInsets.symmetric(vertical: 0),
//                                child: Text(" CSGNY IT Consuluting Pvt Ltd.",style: TextStyle(
//                                    fontSize: 11 ,fontWeight: FontWeight.normal,
//                                    color: Colors.black)),
//                              ),

                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Center(
                                      child: Container(
                                        height: 40,
                                        width: 150,
                                        decoration: new BoxDecoration(
                                            borderRadius: BorderRadius.circular(5),
                                            border: new Border.all(color: Color(0xFFEB5757))
                                        ),
                                        child: Center(child: Text("  About Employer  ",style: TextStyle(
                                            fontSize: 14 ,fontWeight: FontWeight.normal,
                                            color: Color(0xFFEB5757))
                                        )),
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                              SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 12),
                                      child: Row(
                                        children: <Widget>[
                                          Stack(
                                            children: <Widget>[
                                              Text("Hourly Rate",style: TextStyle(
                                                  fontSize: 14,fontWeight: FontWeight.normal,
                                                  color: Colors.black)
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(left:128.0),
                                                child: Text("\$10",style: TextStyle(
                                                    fontSize: 14 ,fontWeight: FontWeight.normal,
                                                    color: Colors.black)
                                                ),
                                              ),
                                            ],
                                          ),

                                        ],
                                      ),
                                    ),



                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 12),
                                child: Row(
                                  children: <Widget>[
                                    Stack(
                                      children: <Widget>[
                                        Text("Odd Hourly Rate",style: TextStyle(
                                            fontSize: 14,fontWeight: FontWeight.normal,
                                            color: Colors.black)
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left:128.0),
                                          child: Text("\$14",style: TextStyle(
                                              fontSize: 14 ,fontWeight: FontWeight.normal,
                                              color: Colors.black)
                                          ),
                                        ),
                                      ],
                                    ),

                                  ],
                                ),
                              ),

                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 12),
                                child: Row(
                                  children: <Widget>[
//                                    Stack(
//                                      children: <Widget>[
//                                        Text("Open Slots",style: TextStyle(
//                                            fontSize: 14,fontWeight: FontWeight.normal,
//                                            color: Colors.black)
//                                        ),
//                                        Padding(
//                                          padding: const EdgeInsets.only(left:128.0),
//                                          child: Text("\$10",style: TextStyle(
//                                              fontSize: 14 ,fontWeight: FontWeight.normal,
//                                              color: Colors.black)
//                                          ),
//                                        ),
//                                      ],
//                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 0.0),
                                      child: Stack(
                                        children: <Widget>[
                                          Text("Required",style: TextStyle(
                                              fontSize: 14,fontWeight: FontWeight.normal,
                                              color: Colors.black)
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left:130.0),
                                            child: Text("2",style: TextStyle(
                                                fontSize: 14 ,fontWeight: FontWeight.normal,
                                                color: Colors.black)
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),

                                  ],
                                ),
                              ),

                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 12),
                                child: Row(
                                  children: <Widget>[
                                    Stack(
                                      children: <Widget>[
                                        Text("Project Dates",style: TextStyle(
                                            fontSize: 14,fontWeight: FontWeight.normal,
                                            color: Colors.black)
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left:128.0),
                                          child: Row(
                                            children: <Widget>[
                                              Text("14 july 2017",style: TextStyle(
                                                  fontSize: 14 ,fontWeight: FontWeight.normal,
                                                  color: Colors.black)
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 12),
                                                child: Text("To",style: TextStyle(
                                                    fontSize: 14 ,fontWeight: FontWeight.normal,
                                                    color: Colors.black)
                                                ),
                                              ),
                                              Text("25 july 2017",style: TextStyle(
                                                  fontSize: 14 ,fontWeight: FontWeight.normal,
                                                  color: Colors.black)
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),

                                  ],
                                ),
                              ),

                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 12),
                                child: Row(
                                  children: <Widget>[
                                    Stack(
                                      children: <Widget>[
                                        Text("Work Hours",style: TextStyle(
                                            fontSize: 14,fontWeight: FontWeight.normal,
                                            color: Colors.black)
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left:128.0),
                                          child: Row(
                                            children: <Widget>[
                                              Text("06:00 AM",style: TextStyle(
                                                  fontSize: 14 ,fontWeight: FontWeight.normal,
                                                  color: Colors.black)
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 12),
                                                child: Text("To",style: TextStyle(
                                                    fontSize: 14 ,fontWeight: FontWeight.normal,
                                                    color: Colors.black)
                                                ),
                                              ),
                                              Text("06:00 PM",style: TextStyle(
                                                  fontSize: 14 ,fontWeight: FontWeight.normal,
                                                  color: Colors.black)
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),

                                  ],
                                ),
                              ),


                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 12),
                                child: Row(
                                  children: <Widget>[
                                    Stack(
                                      children: <Widget>[
                                        Text("Odd Hours",style: TextStyle(
                                            fontSize: 14,fontWeight: FontWeight.normal,
                                            color: Colors.black)
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left:128.0),
                                          child: Row(
                                            children: <Widget>[
                                              Text("07:00 PM",style: TextStyle(
                                                  fontSize: 14 ,fontWeight: FontWeight.normal,
                                                  color: Colors.black)
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 12),
                                                child: Text("To",style: TextStyle(
                                                    fontSize: 14 ,fontWeight: FontWeight.normal,
                                                    color: Colors.black)
                                                ),
                                              ),
                                              Text("10:00 PM",style: TextStyle(
                                                  fontSize: 14 ,fontWeight: FontWeight.normal,
                                                  color: Colors.black)
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),

                                  ],
                                ),
                              ),

                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 12),
                                child: Row(
                                  children: <Widget>[
                                    Stack(
                                      children: <Widget>[
                                        Text("Weekend Work",style: TextStyle(
                                            fontSize: 14,fontWeight: FontWeight.normal,
                                            color: Colors.black)
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left:128.0),
                                          child: Text("Yes",style: TextStyle(
                                              fontSize: 14 ,fontWeight: FontWeight.normal,
                                              color: Colors.black)
                                          ),
                                        ),
                                      ],
                                    ),

                                  ],
                                ),
                              ),

                            ],
                          ),
                        ),



                            ],
                          ),
                        ),

                        Container(
                            height: 40,color: Color(0xFFE8E8E8),
                            child: Padding(
                              padding: const EdgeInsets.only(left:25.0,right: 25),
                              child: Center(child: Row(
                                children: <Widget>[
                                  Text("Job Description",style: new TextStyle(
                                      fontSize: 16.0,
                                      color: Colors.black,fontWeight: FontWeight.bold
                                  ),),

                                ],
                              )),
                            )),
                        Padding(
                          padding: const EdgeInsets.all( 22),
                          child: Text("We are looking for a hardworking workers who are willing to work in construction. This is for a reputed client, the worker can choose to work on weekends or Odd hours too. Apply if you like to make money for short time work",style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.black,fontWeight: FontWeight.normal
                          ),),
                        ),

                        Container(
                            height: 40,color: Color(0xFFE8E8E8),
                            child: Padding(
                              padding: const EdgeInsets.only(left:25.0,right: 25),
                              child: Center(child: Row(
                                children: <Widget>[
                                  Text("Location",style: new TextStyle(
                                      fontSize: 16.0,
                                      color: Colors.black,fontWeight: FontWeight.bold
                                  ),),

                                ],
                              )),
                            )),
                        Padding(
                          padding: const EdgeInsets.all( 22),
                          child: Column(
                            children: <Widget>[
                              Text("37-24 24th Street Suite 340 Long Island City, NY 11101"
                                ,style: new TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,fontWeight: FontWeight.normal
                                ),),

                            ],
                          ),
                        ),

                        Container(
                          width: MediaQuery.of(context).size.width,
                            child: Image.asset("assets/loc.png",fit: BoxFit.fitWidth,)
                        ),

                        Padding(
                          padding: const EdgeInsets.only(left:65.0,right: 65,top: 40),
                          child: GestureDetector(
                            onTap: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext   context) => new thanks(),
                                ),
                              );
//                              Navigator.pop(context);
//                              _ShowDialog();
//                    Navigator.push(
//                      context,
//                      MaterialPageRoute(
//                        builder: (BuildContext   context) => new jobdetails(),
//                      ),
//                    );
                            },
                            child: Container(
                              height: 50,
                              decoration: new BoxDecoration(
                                //shape:BoxShape.circle ,
//                  borderRadius: BorderRadius.circular(30.0),
                                gradient: const LinearGradient(
                                  begin: FractionalOffset.centerRight,
                                  end: FractionalOffset.centerLeft,
                                  colors: <Color>[
                                    const Color(0xFFFF473A),
                                    const Color(0xFFFF473A),
                                  ],
                                ),
                              ),
                              child: Center(
                                child: Text("Apply Now",style: TextStyle(
                                    fontSize: 16 ,fontWeight: FontWeight.bold,
                                    color: Color(0xFFFFFFFF))),
                              ),


                            ),
                          ),
                        ),
                        Container(height: 50,),

                      ],
                    ),
                  ),
                  Container(height: 15,),
                ],
              ),
            ),
          )),
    );
  }

  void _ShowDialog() {

    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          contentPadding: EdgeInsets.all(0.0),
          backgroundColor: Colors.white,
          // title: new Text("$value",style: TextStyle(fontSize: 24.0,color: Colors.purpleAccent),),
          content: Stack(
            children: <Widget>[
              Center(child: Image.asset("assets/tq.png")),
              Center(child: Image.asset("assets/tq1.png"))
            ],
          ),
          actions: <Widget>[

          ],

        );
      },
    );
  }
}
