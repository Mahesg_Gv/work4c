import 'package:flutter/material.dart';
import 'package:work4c/sidebar/sidebar.dart';

import 'cmpDetails.dart';
import 'list.dart';

class cmpFilter extends StatefulWidget {
  @override
  _cmpFilterState createState() => _cmpFilterState();
}

class _cmpFilterState extends State<cmpFilter> {
  double _lowerValue = 10.0;
  double _upperValue = 20.0;
  bool c1 =false;
  bool c2 =true;
  bool c3 =false;
  bool isSwitched = true;
  bool isSwitched1 = true;
  bool isSwitched2 = true;
  bool isChecked = false;
  bool isChecked1 = false;
  String dropdownValue = 'Bangalore';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xFF3F0361),
        title: Text("Sord and Filter"),
        actions: <Widget>[
          IconButton(icon: new Icon(Icons.close),
            onPressed: (){
              Navigator.pop(context);
              // _showMaterialSearch(context);
            },
          ),
        ],
      ),
      drawer:  MyDrawer(),
//      new Drawer(
//        child: Container(
//          color: Colors.black,
//          child: new ListView(
//            children: <Widget>[
//              Padding(
//                padding: const EdgeInsets.only(left:20.0,top: 25),
//                child: Row(
//                  children: <Widget>[
//                    ClipOval(
//                        child:Image.asset("assets/dp.png",height: 100,
//                          width: 100,fit: BoxFit.fill,)
//                    ),
//
////                child: new UserAccountsDrawerHeader(
////                  accountEmail: new Text("thetechnowb@gmail.com"),
////                  accountName: new Text("Tarun Joshi"),
////                  currentAccountPicture: new CircleAvatar(
////                    backgroundColor: Colors.white,
////                    child: new Text("T"),
////                  ),
//////                  otherAccountsPictures: <Widget>[
//////                    new CircleAvatar(
//////                      backgroundColor: Colors.white,
//////                      child: new Text("J"),
//////                    ),
//////                    new CircleAvatar(
//////                      backgroundColor: Colors.white,
//////                      child: new Text("P"),
//////                    ),
//////                  ],
////                ),
//
//                    Padding(
//                      padding: const EdgeInsets.only(left:15.0),
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        children: <Widget>[
//                          Row(
//                            children: <Widget>[
//                              new Text("Musa Ahmed",style: TextStyle(color: Colors.white),),
//                              Padding(padding: EdgeInsets.only(left: 10)),
//                              new Icon(Icons.edit,color: Colors.white,)
//                            ],
//                          ),
//                          new Text("musa@zamask.com",style: TextStyle(color: Colors.white),),
//                          new Text("+91 - 999999999",style: TextStyle(color: Colors.white),),
//                        ],
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//              Container(
//                height: 100,
//              ),
//              new ListTile(
//                title: Row(
//                  children: <Widget>[
//                    new Icon(Icons.notifications_none,color: Colors.white),
//                    Padding(padding: EdgeInsets.only(left: 8)),
//                    new Text("My Notification",style: TextStyle(color: Colors.white),),
//                  ],
//                ),
//              ),
//              new ListTile(
//                title: Row(
//                  children: <Widget>[
//                    new Icon(Icons.library_books,color: Colors.white,),
//                    Padding(padding: EdgeInsets.only(left: 8)),
//                    new Text("My Timesheets",style: TextStyle(color: Colors.white),),
//                  ],
//                ),
//              ),
//              new Divider(height: 5.0),
//              new ListTile(
//                title: Row(
//                  children: <Widget>[
//                    new Icon(Icons.content_paste,color: Colors.white),
//                    Padding(padding: EdgeInsets.only(left: 8)),
//                    new Text("MY Jobs (9)",style: TextStyle(color: Colors.white),),
//                  ],
//                ),
//
//              ),
//              new ListTile(
//                title: Row(
//                  children: <Widget>[
//                    new Icon(Icons.favorite_border,color: Colors.white),
//                    Padding(padding: EdgeInsets.only(left: 8)),
//                    new Text("Company I Follow (2)",style: TextStyle(color: Colors.white),),
//                  ],
//                ),
//
//              )
//            ],
//          ),
//        ),
//      ),


      body: ListView(
        children: <Widget>[
          Container(
              height: 40,color: Color(0xFFE8E8E8),
              child: Padding(
                padding: const EdgeInsets.only(left:25.0,right: 25),
                child: Center(child: Row(
                  children: <Widget>[
                    Text("Sort By",style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.black
                    ),),
                  ],
                )),
              )),

          Padding(
            padding: const EdgeInsets.only(left:29.0,right: 29,top: 30,bottom: 30),
            child: Container(
              height: 45,
              decoration: new BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: new Border.all(color: Color(0xFFEB5757))
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      setState(() {
                        c1 =false;
                        c2=true;
                        c3=false;
                      });
                    },
                    child: Container(
                        decoration: new BoxDecoration(
                          borderRadius: BorderRadius.only(bottomLeft: const Radius.circular(5.0),topLeft: const Radius.circular(5.0) ),
                          border: new Border.all(color: Colors.transparent),
                          color:c1 ? Colors.red : Colors.white,
                        ),
                        width: MediaQuery.of(context).size.width/ 3 -60/3  ,

                        child: Center(child: Text("Jobs",style: new TextStyle(fontSize: 14.0,color: c1 ?Colors.white: Colors.black),))
                    ),
                  ),

                  GestureDetector(
                    onTap: (){
                      setState(() {
                        c2 =true;
                        c1 =false;
                        c3=false;
                      });
                    },
                    child: Container(
                        width: MediaQuery.of(context).size.width/ 3 -60/3  ,
                        color:c2 ? Colors.red : Colors.white,
                        child: Center(child: Text("Follower",style: new TextStyle(fontSize: 14.0,color: c2 ?Colors.white: Colors.black),))
                    ),
                  ),

                  GestureDetector(
                    onTap: (){
                      setState(() {
                        c3 =true;
                        c1 =false;
                        c2=false;
                      });
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width/ 3 -60/3  ,
                      decoration: new BoxDecoration(
                        borderRadius: BorderRadius.only(bottomRight: const Radius.circular(5.0),topRight: const Radius.circular(5.0) ),
                        border: new Border.all(color: Colors.transparent),
                        color:c3 ? Colors.red : Colors.white,
                      ),
                      child: Center(child: Text("Views",style: new TextStyle(fontSize: 14.0,color: c3 ?Colors.white: Colors.black))
                      ),
                    ),

                  )
                ],
              ),
            ),
          ),

          Container(
              height: 40,color: Color(0xFFE8E8E8),
              child: Padding(
                padding: const EdgeInsets.only(left:25.0,right: 25),
                child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Filter",style: new TextStyle(
                            fontSize: 14.0,
                            color: Colors.black
                        ),),

                        GestureDetector(
                          onTap: (){
                            setState(() {
                              _lowerValue = 10.0;
                              _upperValue = 20.0;
                              c1 =true;
                              c2 =false;
                              c3 =false;
                              isSwitched = true;
                              isSwitched1 = true;
                              isSwitched2 = true;
                              isChecked = false;
                              isChecked1 = false;
                              dropdownValue = 'Bangalore';
                            });
                          },
                          child: Text("Clear All",style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.red
                          ),),
                        ),
                      ],
                    )),
              )),


          Container(height: 20,),
          Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left:25.0,right: 25),
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Only Companies ",style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.black
                          ),),
                          Text("I Follow",style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.black
                          ),),
                        ],
                      ),
                    ),
                    Switch(
                      value: isSwitched2,
                      onChanged: (value) {
                        setState(() {
                          isSwitched2 = value;
                        });
                      },
                      activeTrackColor: Color(0xFFEB5757),
                      activeColor: Colors.red,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15),
                child: Container(height: 1,color: Color(0xFFBCBCBC),),
              ),
              Padding(
                padding:  const EdgeInsets.only(left:25.0,right: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width*0.65,
                      height: 80,
                      color: Color(0xFFEB5757),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text("loc"),
                      ),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Locations",style: new TextStyle(
                            fontSize: 14.0,
                            color: Colors.black
                        ),),
                        GestureDetector(
                          onTap: (){
                            Navigator.push(
                                context,
                                MaterialPageRoute(builder: (BuildContext context) => BookFinderPage(text:"Locations"))
                            );
                          },
                          child: Container(
                              width: MediaQuery.of(context).size.width*0.65,
                              height: 30,
                              color: Colors.grey,
                              child:  Row(
                                children: <Widget>[
                                  Icon(Icons.arrow_drop_down,size: 25,),
                                  Padding(
                                    padding: const EdgeInsets.only(left:25.0),
                                    child: Text("[ select ]"),
                                  ),
                                ],
                              )
//                            Padding(
//                              padding: const EdgeInsets.only( left:80.0),
//                              child: DropdownButton<String>(
//                                value: dropdownValue,
//                                onChanged: (String newValue) {
//                                  setState(() {
//                                    dropdownValue = newValue;
//                                  });
//                                },
//                                items: <String>['Pune','Bangalore', 'San fransico', 'Delhi']
//                                    .map<DropdownMenuItem<String>>((String value) {
//                                  return DropdownMenuItem<String>(
//                                    value: value,
//                                    child: Text(value),
//                                  );
//                                })
//                                    .toList(),
//                              ),
//                            ),
                          ),
                        ),
                      ],
                    ),

                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Container(height: 1,color: Color(0xFFBCBCBC),),
              ),

              Padding(
                padding:  const EdgeInsets.only(left:25.0,right: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width*0.65,
                      height: 80,
                      color: Color(0xFFEB5757),
                      child: Padding(
                        padding: const EdgeInsets.all( 8),
                        child: Text("Carpenter, Electrician, Construction, Waiter, Security"),
                      ),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Type of work",style: new TextStyle(
                            fontSize: 14.0,
                            color: Colors.black
                        ),),
                        GestureDetector(
                          onTap: (){
                            Navigator.push(
                                context,
                                MaterialPageRoute(builder: (BuildContext context) => BookFinderPage(text:"Type of work"))
                            );
                          },
                          child: Container(
                              width: MediaQuery.of(context).size.width*0.65,
                              height: 30,
                              color: Colors.grey,
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.arrow_drop_down,size: 25,),
                                  Padding(
                                    padding: const EdgeInsets.only(left:25.0),
                                    child: Text("[ select ]"),
                                  ),
                                ],
                              )
//                            Padding(
//                              padding: const EdgeInsets.only( left:80.0),
//                              child: DropdownButton<String>(
//                                value: dropdownValue,
//                                onChanged: (String newValue) {
//                                  setState(() {
//                                    dropdownValue = newValue;
//                                  });
//                                },
//                                items: <String>['Pune', 'Bangalore', 'San fransico', 'Delhi']
//                                    .map<DropdownMenuItem<String>>((String value) {
//                                  return DropdownMenuItem<String>(
//                                    value: value,
//                                    child: Text(value),
//                                  );
//                                })
//                                    .toList(),
//                              ),
//                            ),
                          ),
                        ),
                      ],
                    ),

                  ],
                ),
              ),



            ],
          ),

          Padding(
            padding: const EdgeInsets.only(left:65.0,right: 65,top: 40),
            child: GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext   context) => new cmpDetails(),
                  ),
                );
              },
              child: Container(
                height: 42,
                decoration: new BoxDecoration(
                  //shape:BoxShape.circle ,
//                  borderRadius: BorderRadius.circular(30.0),
                  gradient: const LinearGradient(
                    begin: FractionalOffset.centerRight,
                    end: FractionalOffset.centerLeft,
                    colors: <Color>[
                      const Color(0xFFFF473A),
                      const Color(0xFFFF473A),
                    ],
                  ),
                ),
                child: Center(
                  child: Text("Apply Filters",style: TextStyle(
                      fontSize: 13 ,fontWeight: FontWeight.bold,
                      color: Color(0xFFFFFFFF))),
                ),


              ),
            ),
          ),
          Container(height: 30,),

        ],
      ),
    );
  }
}
