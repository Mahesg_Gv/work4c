import 'package:flutter/material.dart';
import 'package:work4c/home/notifnIcon.dart';
import 'package:work4c/sidebar/sidebar.dart';

import 'cmpJobCard.dart';

class cmpDetails extends StatefulWidget {
  @override
  _cmpDetailsState createState() => _cmpDetailsState();
}

class _cmpDetailsState extends State<cmpDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(" CSGNY IT Consuluting Pvt Ltd."),
        backgroundColor: Color(0xFF19ACCA),
        elevation: 0,
        actions: <Widget>[
          ntfnIcon(),
        ],

      ),
//        floatingActionButtonLocation: FloatingActionButtonLocation.miniStartTop,
//        floatingActionButton:  Padding(
//          padding: const EdgeInsets.only(top:100.0),
//          child: Container(
//            width: 35.0,
//            height: 35.0,
//            child: FloatingActionButton(
//              backgroundColor: Color(0xFF19ACCA),
//              elevation: 0,
//              onPressed: () {
//                Navigator.pop(context);
//              },
//              child: Icon(Icons.arrow_back,),
////          shape: RoundedRectangleBorder(),
////              mini: true,
//              // heroTag: "demoTag",
//            ),
//          ),
//        ),
//        drawer: MyDrawer(),
      body:  ListView(
        children: <Widget>[
          Container(
            color: Color(0xFF19ACCA),
              child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Text("CSGNY IT Consuluting Pvt Ltd.",style: TextStyle(
                    fontSize: 16 ,fontWeight: FontWeight.bold,
                    color: Color(0xFFFFFFFF))),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: Text("CSG IT Consulting receives unique consulting job opportunities from several leading employers and vendors",style: TextStyle(
                    fontSize: 13 ,fontWeight: FontWeight.normal,
                    color: Color(0xFFF2F2F2),),textAlign: TextAlign.start,),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40,vertical: 10),
                child: Text("Work Types: Carpenter, Electrician, Construction, Waiter, Security "
                  ,style: TextStyle(
                    fontSize: 13 ,fontWeight: FontWeight.normal,
                    color: Color(0xFFF2F2F2),),textAlign: TextAlign.left,),
              ),


              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Jobs",textAlign: TextAlign.center
                      ,style: TextStyle(
                          fontSize: 14 ,fontWeight: FontWeight.bold,
                          color: Color(0xFFFFFFFF))),
                  Padding(
                    padding: const EdgeInsets.only(left:18.0),
                    child: Text("50",textAlign: TextAlign.center
                        ,style: TextStyle(
                            fontSize: 14 ,fontWeight: FontWeight.bold,
                            color: Color(0xFFFFFFFF))),
                  ),

                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15),
                child: Container(height: 28,
                  width: 112,
                  child: OutlineButton(
                    borderSide: BorderSide(
                      color: Colors.white, //Color of the border
                      style: BorderStyle.solid, //Style of the border
                      width: 0.8, //width of the border
                    ),
                    color: Colors.redAccent,
                    onPressed: (){

                    },
                    child: Text("Following" ,style: TextStyle(
                        fontSize: 14 ,fontWeight: FontWeight.normal,
                        color: Color(0xFFFFFFFF))),

                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.web,color: Colors.white,),
                        Text("  csgitconsulting.com",style: new TextStyle(
                            fontSize: 12.0,
                            color: Colors.white
                        ),),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.location_on,color: Colors.white,),
                        Text("  San Francisco. CA",style: new TextStyle(
                            fontSize: 12.0,
                            color: Colors.white
                        ),),
                      ],
                    ),
                  ],
                ),
              ),
            Container(height: 30,)
            ],
          )),
          cmpjobcard(),
          cmpjobcard(),
          cmpjobcard(),
          cmpjobcard(),
          cmpjobcard(),
          cmpjobcard(),
          Container(height: 20,)
        ]
      )
    );
  }
}
