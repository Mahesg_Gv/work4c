import 'package:flutter/material.dart';

class BookFinderPage extends StatefulWidget {
  final String text;

//  https://indian-cities-api-nocbegfhqg.now.sh/cities

  // In the constructor, require a Todo.
  BookFinderPage({Key key, @required this.text}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<BookFinderPage> {

  List<bool> inputs = new List<bool>();
  @override
  void initState() {
    // TODO: implement initState
    setState(() {
      for(int i=0;i<20;i++){
        inputs.add(false);
      }
    });
  }

  void ItemChange(bool val,int index){
    setState(() {
      inputs[index] = val;
    });
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Color(0xFF3F0361),
        title: new Text("${widget.text}"),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Align(
            alignment: FractionalOffset.topCenter,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 20),
              child: Container(
                height: 35,
                decoration: new BoxDecoration(
//                    shape: BoxShape.circle,// You can use like this way or like the below line
                  borderRadius: new BorderRadius.circular(30.0),
                  color: Colors.white,
                    border: Border.all(color: Colors.black)
                ),
                child: TextField(
                  style: new TextStyle(
                      fontSize: 18.0,
                      color: Colors.black,fontWeight: FontWeight.w500
                  ),
//            controller: editingController,
                  decoration: InputDecoration(
//                labelText: "Search",
                    hintText: "Search Jobs",
                    hintStyle: TextStyle(fontSize: 12,fontWeight: FontWeight.bold, color: Colors.black),
                    prefixIcon: Icon(Icons.search,color: Colors.black,),
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),),
          Padding(
            padding: const EdgeInsets.only(top:50.0),
            child: Container(
              child: new ListView.builder(
                  itemCount: inputs.length,
                  itemBuilder: (BuildContext context, int index){
                    return new Card(
                      child: new Container(
                        padding: new EdgeInsets.all(10.0),
                        child: new Column(
                          children: <Widget>[
                            new CheckboxListTile(
                                value: inputs[index],
                                title: new Text('item ${index}'),
                                controlAffinity: ListTileControlAffinity.leading,
                                onChanged:(bool val){ItemChange(val, index);}
                            )
                          ],
                        ),
                      ),
                    );

                  }
              ),
            ),
          ),
          Align(
              alignment: FractionalOffset.bottomCenter,
              child:Padding(
                padding: const EdgeInsets.all(20.0),
                child: GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: 42,
                    width: 100,
                    decoration: new BoxDecoration(
                      //shape:BoxShape.circle ,
                      borderRadius: BorderRadius.circular(30.0),
                      gradient: const LinearGradient(
                        begin: FractionalOffset.centerRight,
                        end: FractionalOffset.centerLeft,
                        colors: <Color>[
                          const Color(0xFFFF473A),
                          const Color(0xFFFF473A),
                        ],
                      ),
                    ),
                    child: Center(
                      child: Text("Done",style: TextStyle(
                          fontSize: 14 ,fontWeight: FontWeight.bold,
                          color: Color(0xFFFFFFFF))),
                    ),


                  ),
                ),
              ),)


        ],
      ),
    );
  }
}