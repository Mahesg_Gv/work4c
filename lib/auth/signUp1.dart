import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../main.dart';

class signUp1 extends StatelessWidget {

  bool _obscureText = false;
  final otp = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.miniStartTop,
      floatingActionButton:  Padding(
        padding: const EdgeInsets.only(top:100.0),
        child: Container(
          width: 35.0,
          height: 35.0,
          child: FloatingActionButton(
            backgroundColor: Colors.transparent,
            elevation: 0,
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back,),
//          shape: RoundedRectangleBorder(),
            mini: true,
            // heroTag: "demoTag",
          ),
        ),
      ),


     body: Form(
       key: _formKey,
       child: Container(
         height: MediaQuery.of(context).size.height,
         decoration: new BoxDecoration(
           //shape:BoxShape.circle ,
//                  borderRadius: BorderRadius.circular(30.0),
           gradient: const LinearGradient(
             begin: FractionalOffset.topCenter,
             end: FractionalOffset.bottomCenter,
             colors: <Color>[
               const Color(0xFFC34747),
               const Color(0xFF3F0361),
             ],
           ),
         ),
           child: SingleChildScrollView(
               child: Column(
                 children: <Widget>[
                   Padding(
                     padding: const EdgeInsets.only(top: 40,bottom: 22),
                     child: Text("Sign Up",style: TextStyle(
                         fontSize: 20 ,fontWeight: FontWeight.bold,
                         color: Color(0xFFFFFFFF))),
                   ),

                   Padding(
                     padding: const EdgeInsets.only(left:0.0,right: 0,bottom: 6),
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                         Container(
                             width: 15.0,
                             height: 15.0,
                             decoration: new BoxDecoration(
                               shape: BoxShape.circle,// You can use like this way or like the below line
                               //borderRadius: new BorderRadius.circular(30.0),
                               color: Colors.white,
                             ),
                             child: Center(child: Text("1"))
                         ),
                         Container(
                           width: 200,
                           height: 1,
                           color: Colors.white,
                         ),
                         Container(
                             width: 15.0,
//                          height: 15.0,
                             decoration: new BoxDecoration(
//                             borderRadius: new BorderRadius.circular(25.0),
//                             border: new Border.all(
//                               width: 1.0,
//                               color: Color(0xFFBDBDBD),
//                             ),
                                shape: BoxShape.circle,// You can use like this way or like the below line
                               //borderRadius: new BorderRadius.circular(30.0),
                               color: Colors.white,
                             ),
                             child: Center(child: Text("2"))
                         ),
                       ],
                     ),
                   ),

                   Padding(
                     padding: const EdgeInsets.all(15.0),
                     child: Container(
                       height: 50,
                       color: Colors.black26,
                       child: Center(
                           child: Text("Verification",style: TextStyle(fontSize: 14, color: Colors.white),)
                       ),
                     ),
                   ),

                   Padding(
                     padding: const EdgeInsets.only(top:10.0,bottom: 10),
                     child: Column(
                       children: <Widget>[
                         Text("Enter the code",style: TextStyle(fontSize: 14, color: Colors.white),),
                         Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           children: <Widget>[
                             Text("from ",style: TextStyle(fontSize: 14, color: Colors.white),),
                             Text("SMS",style: TextStyle(fontSize: 14, color: Colors.white,fontWeight: FontWeight.bold),),
                             Text(" we just sent to you",style: TextStyle(fontSize: 14, color: Colors.white),),
                           ],
                         ),

                       ],
                     ),
                   ),

                   Padding(
                     padding: const EdgeInsets.only(bottom:58.0),
                     child: Container(
                       width: 200,
                       child: TextFormField(textAlign: TextAlign.center,
                         style: TextStyle(color: Colors.white,),
                         decoration: InputDecoration(
                           border: new UnderlineInputBorder(
                               borderSide: new BorderSide(
                                   color: Colors.red
                               )
                           ),
                           fillColor: Colors.white,
                           hintText: 'XXXX',
                           hintStyle: TextStyle(fontSize: 12, color: Colors.white,),

                         ),
//                     controller: _usernameController,
                       ),
                     ),
                   ),
//                 Container(height: 1,width: 100,color: Colors.white,),


                   Padding(
                     padding: const EdgeInsets.only(left:65.0,right: 65),
                     child: GestureDetector(
                       onTap: (){
                         _formKey.currentState.validate()?
                         Navigator.push(
                           context,
                           MaterialPageRoute(
                             builder: (BuildContext   context) => new HomePage(tab: 0,follow : 1),
                           ),
                         ):
                         Fluttertoast.showToast(msg: "Enter OTP ");
                       },
                       child: Container(
                         height: 50,
                         decoration: new BoxDecoration(
                           //shape:BoxShape.circle ,
//                  borderRadius: BorderRadius.circular(30.0),
                           gradient: const LinearGradient(
                             begin: FractionalOffset.centerRight,
                             end: FractionalOffset.centerLeft,
                             colors: <Color>[
                               const Color(0xFFFF473A),
                               const Color(0xFFFF473A),
                             ],
                           ),
                         ),
                         child: Center(
                           child: Text("Done",style: TextStyle(
                               fontSize: 16 ,fontWeight: FontWeight.bold,
                               color: Color(0xFFFFFFFF))),
                         ),


                       ),
                     ),
                   ),
                 ],
               )),
       ),
     ),
//          controller: _usernameController,
    );
  }

  String validateotp(String value) {
    if (value.length < 4)
      return 'Enter OTP';
    else
      return null;
  }
}
