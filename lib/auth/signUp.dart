import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'signUp1.dart';

class singup extends StatefulWidget {
  @override
  _singupState createState() => _singupState();
}
class skill {
  const skill(this.id,this.name);

  final String name;
  final int id;
}

class Price {
  const Price(this.id,this.price);

  final String price;
  final int id;
}
class shift {
  String text;
  int index;
  shift({this.text, this.index});
}


class company {
  String text;
  int index;
  company({this.text, this.index});
}


class _singupState extends State<singup> {

  List<String> _colors = <String>['', 'red', 'green', 'blue', 'orange'];
  String _color = '';
  String Skill = "Skills";

  skill selectedUser;
  Price selectedprice;
  List<skill> users = <skill>[const skill(1,'Foo'), const skill(2,'Bar')];
  List<Price> price = <Price>[const Price(1,'\$10'), const Price(2,'\$12'), const Price(2,'\$15')];
  bool _obscureText = false;
  final _usernameController = TextEditingController();
  DateTime selectedDate = DateTime.now();
  String dob = "dd - mm - yyyy";
  String SKILL ="Skills";
  int _radioValue1 = 0;
  bool isChecked = false;

  int _currVal = 0;
  String _currText = '';

  List<shift> _group = [
    shift(
      text: "Yes",
      index: 1,
    ),
    shift(
      text: "No",
      index: 2,
    ),
  ];
  List<company> _group1 = [
    company(
      text: "Yes",
      index: 1,
    ),
    company(
      text: "No",
      index: 2,
    ),
  ];


  @override
  void initState() {
    selectedUser=users[0];
    selectedprice=price[0];
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        dob ="${selectedDate.day} - ${selectedDate.month} - ${selectedDate.year}";
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.miniStartTop,
      floatingActionButton:  Padding(
        padding: const EdgeInsets.only(top:100.0),
        child: Container(
          width: 35.0,
          height: 35.0,
          child: FloatingActionButton(
            backgroundColor: Colors.transparent,
            elevation: 0,
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back,),
//          shape: RoundedRectangleBorder(),
            mini: true,
            // heroTag: "demoTag",
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: new BoxDecoration(
          //shape:BoxShape.circle ,
//                  borderRadius: BorderRadius.circular(30.0),
          gradient: const LinearGradient(
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
            colors: <Color>[
              const Color(0xFFC34747),
              const Color(0xFF3F0361),
            ],
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[

              Padding(
                padding: const EdgeInsets.only(left:65.0,right: 65),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 40,bottom: 22),
                          child: Text("Sign Up",style: TextStyle(
                              fontSize: 20 ,fontWeight: FontWeight.bold,
                              color: Color(0xFFFFFFFF))),
                        ),

                        Padding(
                          padding: const EdgeInsets.only(left:0.0,right: 0,bottom: 6),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: 15.0,
                                height: 15.0,
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.circle,// You can use like this way or like the below line
                                    //borderRadius: new BorderRadius.circular(30.0),
                                    color: Colors.white,
                                  ),
                                  child: Center(child: Text("1",style: TextStyle(fontSize: 10,color:Colors.black,fontWeight: FontWeight.bold ),))
                              ),
                              Container(
                                  width: 200,
                                  height: 1,
                                color: Color(0xFFBDBDBD),
                              ),
                              Container(
                                width: 15.0,
//                          height: 15.0,
                                  decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.circular(25.0),
                                    border: new Border.all(
                                      width: 1.0,
                                      color: Color(0xFFBDBDBD),
                                    ),
//                              shape: BoxShape.circle,// You can use like this way or like the below line
                                    //borderRadius: new BorderRadius.circular(30.0),
//                              color: Colors.brown,
                                  ),
                                  child: Center(child: Text("2",style: TextStyle(fontSize: 10,color:Color(0xFFBDBDBD),fontWeight: FontWeight.bold ),))
                              ),
                            ],
                          ),
                        ),
                        Container(
                            width: 100.0,
                            height: 100.0,
                            decoration: new BoxDecoration(
                              borderRadius: new BorderRadius.circular(100.0),
                              border: new Border.all(
                                width: 1.0,
                                color: Color(0xFFBDBDBD),
                              ),
//                              shape: BoxShape.circle,// You can use like this way or like the below line
                              //borderRadius: new BorderRadius.circular(30.0),
//                              color: Colors.brown,
                            ),
                            child: Center(
                                child: Icon(Icons.camera_alt,size: 40,color: Colors.white,)
                            )
                        ),
                       Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: Text("Your Profile Picture",style: TextStyle(fontSize: 12,color:Colors.white70 ),),
                       ),

                          TextField(
                            keyboardType: TextInputType.number,
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              border: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Colors.red
                                  )
                              ),
                              fillColor: Colors.white,
                              hintText: 'Your Name',
                              hintStyle: TextStyle( fontSize: 12,color: Colors.white70),
                            ),

                          ),

                          TextField(
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              border: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Colors.red
                                  )
                              ),
                              fillColor: Colors.white,
                              hintText: 'Your Email',
                              hintStyle: TextStyle(fontSize: 12, color: Colors.white70),
                            ),

                          ),


                          Padding(
                            padding: const EdgeInsets.only(top:4.0,bottom: 4),
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                prefixIcon: Container(
                                  height: 4,
                                  color: Colors.black,
                                  child: Center(
                                    widthFactor: 0.0,
                                    child: Text('+91', style: TextStyle(color:Colors.white, )),
                                  ),
                                ),
                                border: new UnderlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: Colors.red
                                    )
                                ),
                                fillColor: Colors.white,
                                hintText: '   Mobile',
                                hintStyle: TextStyle(fontSize: 12, color: Colors.white70),
                              ),
                              controller: _usernameController,
                            ),
                          ),
                          TextFormField(
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              suffixIcon: GestureDetector(
                                onTap: () {
                                  _obscureText = !_obscureText;

                                },
                                child: Icon(
                                  _obscureText ? Icons.visibility : Icons.visibility_off,color:Colors.redAccent,
                                  semanticLabel: _obscureText ? 'show password' : 'hide password',
                                ),
                              ),
                              border: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Colors.red
                                  )
                              ),
                              fillColor: Colors.white,
                              hintText: 'Password',
                              hintStyle: TextStyle(fontSize: 12, color: Colors.white70),
                            ),
                            controller: _usernameController,
                          ),



                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width/2 - 67,
                              child: TextFormField(
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                  border: new UnderlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: Colors.red
                                      )
                                  ),
                                  fillColor: Colors.white,
                                  hintText: 'Street address',
                                  hintStyle: TextStyle(fontSize: 12, color: Colors.white70),
                                ),
                                controller: _usernameController,
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width/2 - 67,
                              child: TextFormField(
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                  border: new UnderlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: Colors.red
                                      )
                                  ),
                                  fillColor: Colors.white,
                                  hintText: 'Zip',
                                  hintStyle: TextStyle(fontSize: 12, color: Colors.white70),
                                ),
                                controller: _usernameController,
                              ),
                            ),

                          ],
                        ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width/2 - 67,
                              child: TextFormField(
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                  border: new UnderlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: Colors.red
                                      )
                                  ),
                                  fillColor: Colors.white,
                                  hintText: 'City',
                                  hintStyle: TextStyle(fontSize: 12, color: Colors.white70),
                                ),
                                controller: _usernameController,
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width/2 - 67,
                              child: TextFormField(
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                  border: new UnderlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: Colors.red
                                      )
                                  ),
                                  fillColor: Colors.white,
                                  hintText: 'State',
                                  hintStyle: TextStyle(fontSize: 12, color: Colors.white70),
                                ),
                                controller: _usernameController,
                              ),
                            ),

                          ],
                        ),
                        GestureDetector(
                          onTap: (){
                            _selectDate(context);
                          },
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: 45,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("Date of birth",style: TextStyle(fontSize: 14, color: Colors.white54),),
                                    Text("$dob",style: TextStyle(fontSize: 14, color: Colors.white54),),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),

                        Container(height: 1,color: Colors.white70,),

                      ],
                    ),
                  ),
                ),
              ),

            Container(height: 50,),

            Container(
              height: 50,
              color: Colors.black26,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left:  20.0),
                    child: Text("Preference",style: TextStyle(fontSize: 14, color: Colors.white70),),
                  ),
                ],
              ),
            ),
              Padding(
                padding: const EdgeInsets.only(left:65.0,right: 65,top: 30),
                child: Column(
                  children: <Widget>[

                    new FormField(
                      builder: (FormFieldState state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                                prefixText: "Skills\t\t\t",
                                prefixStyle: new TextStyle(
                                    color: Colors.white54,fontWeight: FontWeight.bold
                                ),
//                                labelText: 'Color',
                          ),
                          isEmpty: _color == 'select',
                          child: new DropdownButtonHideUnderline(
                            child: new DropdownButton(
                              value: _color,
                              isDense: true,
                              iconSize: 20.0,
                              elevation: 2,
                              style: TextStyle(color: Colors.grey, fontSize: 20),
                              onChanged: (String newValue) {
                                setState(() {
//                            new Contact.favoriteColor = newValue;
                                  _color = newValue;
                                  state.didChange(newValue);
                                  Skill =newValue;
                                });
                              },
                              items: _colors.map((String value) {
                                return new DropdownMenuItem(
                                  value: value,
                                  child: new Text(value,style: TextStyle(
//                                        color: Colors.white,
                                  ),textAlign: TextAlign.right,),
                                );
                              }).toList(),
                            ),
                          ),
                        );
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width/2 - 67,
                          child: Text("Preffered Work time",style: TextStyle(fontSize: 14, color: Colors.white70),)
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width/2 - 67,
                          child: TextFormField(
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              border: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Colors.red
                                  )
                              ),
                              fillColor: Colors.white,
                              hintText: '6.00 AM to 7.00PM',
                              hintStyle: TextStyle(fontSize: 12, color: Colors.white70),
                            ),
                            controller: _usernameController,
                          ),
                        ),

                      ],
                    ),

                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top:10.0, bottom: 6),
                          child: Text("Okay with add time",style: TextStyle(fontSize: 14, color: Colors.white70),),
                        )
                      ],
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Radio(
                          activeColor: Colors.white70,
                          value: 0,
                          groupValue: _radioValue1,
                          onChanged: _handleRadioValueChange1,
                        ),
                        new Text(
                          'Yes',
                          style: new TextStyle(fontSize: 16.0,color: Colors.white70),
                        ),
                        new Radio(
                          activeColor: Colors.white70,
                          value: 1,
                          groupValue: _radioValue1,
                          onChanged: _handleRadioValueChange1,
                        ),
                        new Text(
                          'No',
                          style: new TextStyle(
                            fontSize: 16.0,color: Colors.white70
                          ),
                        ),

                      ],
                    ),

                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top:10.0, bottom: 6),
                          child: Text("Work for companies | don't follow",style: TextStyle(fontSize: 14, color: Colors.white70),),
                        )
                      ],
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Radio(
                          activeColor: Colors.white70,
                          value: 0,
                          groupValue: _radioValue1,
                          onChanged: _handleRadioValueChange1,
                        ),
                        new Text(
                          'Yes',
                          style: new TextStyle(fontSize: 16.0,color: Colors.white70),
                        ),
                        new Radio(
                          activeColor: Colors.white70,
                          value: 1,
                          groupValue: _radioValue1,
                          onChanged: _handleRadioValueChange1,
                        ),
                        new Text(
                          'No',
                          style: new TextStyle(
                            fontSize: 16.0,color: Colors.white70
                          ),
                        ),

                      ],
                    ),



                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Prefered Hourly Rate",style: TextStyle(fontSize: 14, color: Colors.white70),),
                        new DropdownButton<Price>(
                          value: selectedprice,
                          onChanged: (Price newValue) {
                            setState(() {
                              selectedprice = newValue;
                            });
                          },
                          items: price.map((Price _price) {
                            return new DropdownMenuItem<Price>(
                              value: _price,
                              child: new Text(
                                _price.price,
                                style: TextStyle(fontSize: 14, color: Colors.purple),
                              ),
                            );
                          }).toList(),
                        ),
                      ],
                    ),



                    Padding(
                      padding: const EdgeInsets.only(top:48.0),
                      child: GestureDetector(
                        onTap: (){
                          isChecked ?
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext   context) => new signUp1(),
                            ),
                          ): Fluttertoast.showToast(msg: "Accept Terms & Conditions ");
                        },
                        child: Container(
                          height: 50,
                          decoration: new BoxDecoration(
                            //shape:BoxShape.circle ,
//                  borderRadius: BorderRadius.circular(30.0),
                            gradient: const LinearGradient(
                              begin: FractionalOffset.centerRight,
                              end: FractionalOffset.centerLeft,
                              colors: <Color>[
                                const Color(0xFFFF473A),
                                const Color(0xFFFF473A),
                              ],
                            ),
                          ),
                          child: Center(
                            child: Text("Continue",style: TextStyle(
                                fontSize: 16 ,fontWeight: FontWeight.bold,
                                color: Color(0xFFFFFFFF))),
                          ),


                        ),
                      ),
                    ),


                  ],

                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Checkbox(
                    checkColor: Colors.black,
                    activeColor: Colors.white,
                    value: isChecked,
                    onChanged: (value) {
                      setState(() {
                        isChecked = value;
                      });
                    },
                  ),

                  Text("I accept the ",style: TextStyle(fontSize: 14, color: Colors.white70),),
                  Text("Terms & Condition & Privacy Policy",style: TextStyle(decoration: TextDecoration.underline,fontSize: 14, color: Colors.white70,),),
                ],
              ),
              Container(
                height: 50,
              )

//              Row(
//                children: _group
//                    .map((t) => RadioListTile(
//                  title: Text("${t.text}"),
//                  groupValue: _currVal,
//                  value: t.index,
//                  onChanged: (val) {
//                    setState(() {
//                      print(val);
//
//                    });
//                  },
//                ))
//                    .toList(),
//              ),


            ],
          ),
        ),
      ),
    );
  }

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
    });
  }
}
