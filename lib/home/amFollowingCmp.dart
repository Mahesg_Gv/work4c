import 'package:flutter/material.dart';
import 'package:work4c/filter/cmpDetails.dart';
class Followcompanycard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
//      color: Color(0xFF00395A),
          height: 140,
          child: GestureDetector(
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext   context) => new cmpDetails(),
                ),
              );

            },
            child: Card(
              child: Container(
                height: 50,
                color: Color(0xFF00395A),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Text("CSGNY IT Consuluting Pvt Ltd. ",style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.black,fontWeight: FontWeight.bold
                          ),),

                          Container(height: 20,
                            child: OutlineButton(
                              borderSide: BorderSide(
                                color: Colors.white, //Color of the border
                                style: BorderStyle.solid, //Style of the border
                                width: 0.8, //width of the border
                              ),
                              color: Colors.redAccent,
                              onPressed: (){

                              },
                              child: Text("Following",style: TextStyle(color: Colors.white),),

                            ),
                          ),
                          Text("Jobs   10",style: new TextStyle(
                            fontSize: 14.0,
                            color: Colors.black,
                          ),),
                        ],
                      ),
                    ),

                    Container(
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.web,color: Colors.redAccent,),
                                  Text("  csgitconsulting.com",style: new TextStyle(
                                      fontSize: 11.0,
                                      color: Colors.black
                                  ),),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.location_on,color: Colors.redAccent,),
                                  Text("  San Francisco. CA",style: new TextStyle(
                                      fontSize: 11.0,
                                      color: Colors.black
                                  ),),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),


        GestureDetector(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext   context) => new cmpDetails(),
              ),
            );

          },
          child: Container(
//      color: Color(0xFF00395A),
            height: 150,
            child: Card(
              child: Container(
                height: 50,
                color: Color(0xFF00395A),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Text("Zamask Technologies pvt Ltd ",style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.black,
                              fontWeight: FontWeight.bold
                          ),),

                          Container(height: 20,
                            child: OutlineButton(
                              borderSide: BorderSide(
                                color: Colors.white, //Color of the border
                                style: BorderStyle.solid, //Style of the border
                                width: 0.8, //width of the border
                              ),
                              color: Colors.redAccent,
                              onPressed: (){

                              },
                              child: Text("Following",style: TextStyle(color: Colors.white),),

                            ),
                          ),

                        ],
                      ),
                    ),

                    Container(
                      height: 90,
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(Icons.web,color: Colors.redAccent,),
                                      Text("  csgitconsulting.com",style: new TextStyle(
                                          fontSize: 11.0,
                                          color: Colors.black
                                      ),),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(Icons.location_on,color: Colors.redAccent,),
                                      Text("  San Francisco. CA",style: new TextStyle(
                                          fontSize: 11.0,
                                          color: Colors.black
                                      ),),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Text("Jobs",style: new TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black ,fontWeight: FontWeight.normal
                                ),),
                                Text("Ratings",style: new TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black ,fontWeight: FontWeight.normal
                                ),),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Text("10",style: new TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black ,fontWeight: FontWeight.normal
                                ),),
                                Text("Stars",style: new TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black ,fontWeight: FontWeight.normal
                                ),),
                              ],
                            ),
                            Text("Work Types: Construction, Carpenters ",style: new TextStyle(
                                fontSize: 11.0,
                                color: Colors.black ,fontWeight: FontWeight.normal
                            ),),

                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
