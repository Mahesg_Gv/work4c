import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:work4c/sidebar/sidebar.dart';
import 'package:json_table/json_table.dart';

import '../main.dart';

class checkIn extends StatefulWidget {
  @override
  _checkInState createState() => _checkInState();
}

class _checkInState extends State<checkIn> {
  final String jsonSample =
      '[{"Check In":"07:00 am","Check out":"09:00 am","Time Worked":"1 h 0 min","Check Out Reason":"Out of service today"},'
      '{"Check In":"07:00 am","Check out":"09:00 am","Time Worked":"1 h 0 min","Check Out Reason":"Out of service today"},'
      '{"Check In":"07:00 am","Check out":"","Time Worked":"","Check Out Reason":""}]';

//  '[{"Check In":"Ram","email":"ram@gmail.com","age":23,"income":"10Rs","country":"India","area":"abc"}]';



  @override
  Widget build(BuildContext context) {
    var json = jsonDecode(jsonSample);
    bool dlg = false;

    return Scaffold(
      appBar: AppBar(
        title: Text("Checked-In"),
        centerTitle: true,
        backgroundColor: Color(0xFF333333),
        elevation: 0,
        actions: <Widget>[
          IconButton(icon: new Icon(Icons.close),
            onPressed: (){
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (BuildContext   context) => new HomePage(tab: 0,follow : 1),
                ),
              );
              // _showMaterialSearch(context);
            },
          ),
        ],
      ),

      drawer: MyDrawer(),
      body: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          // Box decoration takes a gradient
          gradient: LinearGradient(
            // Where the linear gradient begins and ends
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            // Add one stop for each color. Stops should increase from 0 to 1
            stops: [0.2, 0.9, 1, ],
            colors: [
              // Colors are easy thanks to Flutter's Colors class.
              Color(0xFF333333),
              Color(0xFF4F4F4F),
              Color(0xFF828282),


            ],
          ),
        ),
        child: Stack(
          children: <Widget>[
            dlg ? Center(
              child: Container(
                height: 160,
                width: 1000,
                color: Colors.red,
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Text("Hey, you just left geo fence of your place of work, you have 5 min to return to site and Check-in or you will be checked-out",
                        style: new TextStyle(
                            fontSize: 15.0, color: Colors.white,fontWeight: FontWeight.w500),textAlign: TextAlign.center,),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("00ʰ  05ᵐ  24ˢ",style: new TextStyle(
                          fontSize: 22.0, color: Colors.blue,fontFamily: "Roboto",fontWeight: FontWeight.bold),),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.location_on,color: Colors.blue,),
                          Text("Check-In",style: new TextStyle(
                              fontSize: 24.0, color: Colors.blue,fontWeight: FontWeight.bold),),
                        ],
                      ),
                    ),

                  ],
                ),
              ),
            ): Text(""),
            Column(
              children: <Widget>[
                Text("CSGNY IT Consuluting Pvt Ltd. ",style: new TextStyle(
                  fontSize: 18.0,
                  color: Colors.white ,fontWeight: FontWeight.bold
                   ),),
                Text("Work in site - Construction",style: new TextStyle(
                  fontSize: 14.0,
                  color: Colors.white ,fontWeight: FontWeight.normal
                   ),),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 10,
                        width: 10,

                        decoration: new BoxDecoration(
                          //shape:BoxShape.circle ,
                        borderRadius: BorderRadius.circular(30.0),
                          color: Colors.red,
                          border: new Border.all(
                            color: Colors.white,
                            width: 1,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:8.0),
                        child: Text("Out GeoFence",style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.white ,fontWeight: FontWeight.normal
                           ),),
                      ),
                    ],
                  ),
                ),

                Container(height: 10,),
                Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: (){

                        _checkInDialog();
                      },
                      child: Container(
                          width: MediaQuery.of(context).size.width/2-1,
                        child:Column(
                        children: <Widget>[
                          Icon(Icons.location_on,size: 55,color: Colors.blue,),
                          Text("Check-Out",style: new TextStyle(
                              fontSize: 25.0, color: Colors.blue ,fontWeight: FontWeight.normal),),
                        ],
                      )),
                    ),

                    Padding(
                      padding: const EdgeInsets.only(top:8.0),
                      child: Container(width: 2,height: 100,color: Colors.blue,),
                    ),

                    GestureDetector(
                      onTap: (){
                        _checkOutDialog();
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width/2-1,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text("00ʰ  05ᵐ  24ˢ",style: new TextStyle(
                                    fontSize: 25.0, fontFamily: "Roboto", color: Colors.blue ,fontWeight: FontWeight.normal),),
                              ],
                            ),

                          ],
                        ),

                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top:25.0,bottom: 20),
                      child: Container(width: 100,height: 2,color: Colors.blue,),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top:25.0,bottom: 20),
                      child: Container(width: 100,height: 2,color: Colors.blue,),
                    )
                  ],
                ),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 10),
                  child: Column(
                    children: <Widget>[

                      JsonTable(
                        json,
//                    showColumnToggle: true,
                        tableHeaderBuilder: (String header) {
                          return Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 6.0),
//                        decoration: BoxDecoration(
//                            border: Border.all(width: 0.5),
//                            color: Colors.grey[300]),
                            child: Text(
                              header,
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.display1.copyWith(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16.0,
                                  color: Colors.white),
                            ),
                          );
                        },
                        tableCellBuilder: (value) {
                          return Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 4.0, vertical: 4.0),
//                        decoration: BoxDecoration(
//                            border: Border.all(
//                                width: 0.5,
//                                color: Colors.grey.withOpacity(0.5))),
                            child: Text(
                              value,
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.display1.copyWith(
                                  fontSize: 14.0, color: Colors.white),
                            ),
                          );
                        },
                      ),

                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _checkInDialog() {

    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          elevation: 0,
          contentPadding: EdgeInsets.all(0.0),
          backgroundColor: Color(0xFF333333),
          // title: new Text("$value",style: TextStyle(fontSize: 24.0,color: Colors.purpleAccent),),
          content: Container(
            height: 250,
//            decoration: new BoxDecoration(
//              boxShadow: [BoxShadow(
//                color: Colors.white,
//                blurRadius: 5.0,
//              ),],
//              color: Color(0xFF333333),
////              shape: BoxShape.circle,
//              border: new Border(
//                top: BorderSide(width: 1.0, color: Colors.white),
//                left: BorderSide(width: 1.0, color: Colors.black54),
//                right: BorderSide(width: 1.0, color: Colors.black54),
//                bottom: BorderSide(width: 1.0, color: Colors.white),
//              ),
//            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 10),
              child: Column(
                children: <Widget>[
                Container(
                  child: Text("Hey, you just left geo fence of your place of work, you have 5 min to return to site and Check-in or you will be checked-out",
                    style: new TextStyle(
                    fontSize: 18.0, color: Colors.white,fontWeight: FontWeight.w500),textAlign: TextAlign.center,),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text("05ᵐ  24ˢ",style: new TextStyle(
                      fontSize: 22.0, color: Colors.blue,fontWeight: FontWeight.bold),),
                ),
//              Container(
//                child: Row(
//                  mainAxisAlignment: MainAxisAlignment.center,
//                  children: <Widget>[
//                    Icon(Icons.location_on,color: Colors.blue,),
//                    Text("Check-In",style: new TextStyle(
//                        fontSize: 24.0, color: Colors.blue,fontWeight: FontWeight.bold),),
//                  ],
//                ),
//              ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.location_on,color: Colors.blue,),
                    RaisedButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0)),
                      color: Colors.transparent,
                      elevation: 0,
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      child:Text("Check-In",style: TextStyle(fontSize: 25,color: Colors.blue),),
                    ),
                  ],
                ),

                ],
              ),
            ),
          ),


        );
      },
    );
  }
  void _checkOutDialog() {

    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          elevation: 0,
          contentPadding: EdgeInsets.all(0.0),
          backgroundColor: Color(0xFF333333),
          // title: new Text("$value",style: TextStyle(fontSize: 24.0,color: Colors.purpleAccent),),
          content: Container(
            height: 250,
            width: 1000,
//            decoration: new BoxDecoration(
//              boxShadow: [BoxShadow(
//                color: Colors.white,
//                blurRadius: 5.0,
//              ),],
//              color: Color(0xFF333333),
////              shape: BoxShape.circle,
//              border: new Border(
//                top: BorderSide(width: 1.0, color: Colors.white),
//                left: BorderSide(width: 1.0, color: Colors.black54),
//                right: BorderSide(width: 1.0, color: Colors.black54),
//                bottom: BorderSide(width: 1.0, color: Colors.white),
//              ),
//            ),
            child: Column(
              children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Container(height: 10,),
                    Text("Congradulations!",style: new TextStyle(
                        fontSize: 18.0, color: Colors.white,fontWeight: FontWeight.w500),textAlign: TextAlign.center,),
                    Text(" You have alomost completed with your shift. the current Job has Overtime accept if you like work for Overtime"
                      ,style: new TextStyle(
                      fontSize: 18.0, color: Colors.white,fontWeight: FontWeight.w500),textAlign: TextAlign.center,),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(top:40),
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0)),
                        color: Colors.transparent,
                        elevation: 0,
                        onPressed: (){
                          Navigator.pop(context);
                        },
                        child:Text("Accept",style: TextStyle(fontSize: 24,color: Colors.blue),),
                      ),
                      RaisedButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0)),
                        color: Colors.transparent,
                        elevation: 0,
                        onPressed: (){
                          Navigator.pop(context);

                        },
                        child:Text("Check-Out",style: TextStyle(fontSize: 24,color: Colors.white),),
                      ),
                    ],
                  ),
                ),
              ),

              ],
            ),
          ),


        );
      },
    );
  }
}


class ItemInfo {
  String itemName;
  String itemPrice;

  ItemInfo({
    this.itemName,
    this.itemPrice,
  });
}

var items = <ItemInfo>[
  ItemInfo(
    itemName: 'Item Agg',
    itemPrice: '250',
  ),
  ItemInfo(
    itemName: 'Item B',
    itemPrice: '100',
  ),
  ItemInfo(
    itemName: 'Item C',
    itemPrice: '150',
  ),
];