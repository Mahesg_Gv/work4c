
import 'package:flutter/material.dart';
import 'package:work4c/auth/login.dart';
import 'package:work4c/home/CheckIn.dart';

import '../main.dart';
import '../test.dart';
import 'ex.dart';
import 'myTimesheet.dart';
import 'notification.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {

  bool changep = true;
  String _title="";
  int _page = 0;
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Color(0xFF303030),
        child: new ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left:20.0,top: 25),
              child: Row(
                children: <Widget>[
                  ClipOval(
                      child:Image.asset("assets/dp1.png",height: 100,
                        width: 100,fit: BoxFit.fill,)
                  ),

//                child: new UserAccountsDrawerHeader(
//                  accountEmail: new Text("thetechnowb@gmail.com"),
//                  accountName: new Text("Tarun Joshi"),
//                  currentAccountPicture: new CircleAvatar(
//                    backgroundColor: Colors.white,
//                    child: new Text("T"),
//                  ),
////                  otherAccountsPictures: <Widget>[
////                    new CircleAvatar(
////                      backgroundColor: Colors.white,
////                      child: new Text("J"),
////                    ),
////                    new CircleAvatar(
////                      backgroundColor: Colors.white,
////                      child: new Text("P"),
////                    ),
////                  ],
//                ),

                  Padding(
                    padding: const EdgeInsets.only(left:15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            new Text("Musa Ahmed",style: TextStyle(color: Colors.white),),
                            Padding(padding: EdgeInsets.only(left: 10)),
                            IconButton(icon: Icon(Icons.edit,color: Colors.white,), onPressed: null),
                          ],
                        ),
                        new Text("musa@zamask.com",style: TextStyle(color: Colors.white),),
                        new Text("+91 - 999999999",style: TextStyle(color: Colors.white),),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 100,
            ),
//            new ListTile(
//              onTap: (){
//                setState(() {
//                  changep =true;
//                  Navigator.pop(context);
//
//                });
//              },
//              title: Row(
//                children: <Widget>[
//                  new Icon(Icons.notifications_none,color: Colors.white),
//                  Padding(padding: EdgeInsets.only(left: 8)),
//                  new Text("DashBoadr",style: TextStyle(color: Colors.white),),
//                ],
//              ),
//            ),
            new ListTile(
              onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext   context) => new notification(tab : 0),
                    ),
                  );
//
//                setState(() {
//                  _page = 1;
//                  changep =false;
//                  _title ="Notification";
//                  Navigator.pop(context);
//
//                });
              },
              title: Row(
                children: <Widget>[
                  new Icon(Icons.notifications_none,color: Colors.white),
                  Padding(padding: EdgeInsets.only(left: 8)),
                  new Text("My Notification",style: TextStyle(color: Colors.white),),
                ],
              ),
            ),
            new ListTile(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext   context) => new mySheet(),
                  ),
                );
//                setState(() {
//                  changep =false;
//                  _title ="My Timesheets";
//                  Navigator.pop(context);
//
//                });
              },
              title: Row(
                children: <Widget>[
                  new Icon(Icons.library_books,color: Colors.white,),
                  Padding(padding: EdgeInsets.only(left: 8)),
                  new Text("My Timesheets",style: TextStyle(color: Colors.white),),
                ],
              ),
            ),
            new Divider(height: 5.0),
            new ListTile(
              onTap: (){
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext   context) => new notification(tab : 1),
                  ),
                );
//                setState(() {
//                  changep =false;
//                  _title ="MY Jobs";
//                  Navigator.pop(context);
//
//                });
              },
              title: Row(
                children: <Widget>[
                  new Icon(Icons.content_paste,color: Colors.white),
                  Padding(padding: EdgeInsets.only(left: 8)),
                  new Text("MY Jobs (9)",style: TextStyle(color: Colors.white),),
                ],
              ),

            ),
            new ListTile(
              onTap: (){
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext   context) => ///TileApp(),
                    new HomePage(tab: 1,follow : 0),
                  ),
                );
              },
              title: Row(
                children: <Widget>[
                  new Icon(Icons.favorite_border,color: Colors.white),
                  Padding(padding: EdgeInsets.only(left: 8)),
                  new Text("Company I Follow (2)",style: TextStyle(color: Colors.white),),
                ],
              ),

            ),
            new ListTile(
              onTap: (){
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext   context) => ///TileApp(),
                    new login(),
                  ),
                );
              },
              title: Row(
                children: <Widget>[
                  new Icon(Icons.assignment_ind,color: Colors.white),
                  Padding(padding: EdgeInsets.only(left: 8)),
                  new Text("Logout",style: TextStyle(color: Colors.white),),
                ],
              ),

            )
          ],
        ),
      ),
    );
  }
}