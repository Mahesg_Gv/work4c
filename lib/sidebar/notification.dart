import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:work4c/Notification/myJobs.dart';
import 'package:work4c/Notification/opportunity.dart';
import 'package:work4c/home/companies.dart';
import 'package:work4c/home/jobs.dart';
import 'package:work4c/home/notifnIcon.dart';
import 'package:work4c/sidebar/sidebar.dart';

import '../main.dart';

class notification extends StatefulWidget {
  final int tab;
  notification ({Key key,this.tab}) : super(key: key);

  @override
  _notificationState createState() => _notificationState();
}

class _notificationState extends State<notification> with SingleTickerProviderStateMixin{

  final List<Tab> tabs = <Tab>[
    new Tab(text: "Opportunity"),
    new Tab(text: "    My jobs    "),
  ];
  DateTime currentBackPressTime;

  TabController _tabController;



  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: tabs.length,initialIndex: widget.tab);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar://changep?
      new AppBar(
        backgroundColor: Color(0xFF3F0361),
        title: Text("Notification"),
        actions: <Widget>[
          IconButton(icon: new Icon(Icons.close),
            onPressed: (){
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (BuildContext   context) => new HomePage(tab: 0,follow : 1),
                ),
              );
              // _showMaterialSearch(context);
            },
          ),
        ],
        centerTitle: true,
        bottom: new TabBar(
          labelStyle: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),
          indicatorWeight: 1,
//          labelPadding: EdgeInsets.symmetric(vertical: 10),
          indicatorPadding: EdgeInsets.symmetric(vertical: 50),
          isScrollable: true,
          unselectedLabelColor: Colors.white,
          labelColor: Colors.black,
          indicatorSize: TabBarIndicatorSize.tab,
          indicator: new BubbleTabIndicator(
            indicatorHeight: 25.0,
            indicatorColor: Colors.white,
            tabBarIndicatorSize: TabBarIndicatorSize.tab,
          ),
          tabs: tabs,
          controller: _tabController,

        ),
      ),
//      Container(
//        height: 30,
//        decoration: new BoxDecoration(
//          borderRadius: new BorderRadius.circular(50.0),
//          color: Color(0xFF4c1e66),
//        ),
//        child: new TabBar(
//          isScrollable: true,
//          unselectedLabelColor: Colors.white,
//          labelColor: Colors.black,
//          indicatorSize: TabBarIndicatorSize.tab,
//          indicator: new BubbleTabIndicator(
//            indicatorHeight: 30.0,
//            indicatorColor: Colors.white,
//            tabBarIndicatorSize: TabBarIndicatorSize.tab,
//          ),
//          tabs: tabs,
//          controller: _tabController,
//        ),
//      ),
      drawer: MyDrawer(),

      body:  //Page(),


      Container(
        child:  new TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            new opportunity(),
            new myJobs(),
          ],
          controller: _tabController,
         ),
        //Page(_page),
      ),
    );
  }



}

