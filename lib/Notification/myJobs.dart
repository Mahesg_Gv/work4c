import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'myJobsCard.dart';

class myJobs extends StatefulWidget {
  @override
  _myJobsState createState() => _myJobsState();
}

class _myJobsState extends State<myJobs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(

        decoration: new BoxDecoration(
          //shape:BoxShape.circle ,
//                  borderRadius: BorderRadius.circular(30.0),
          gradient: const LinearGradient(
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
            colors: <Color>[
              const Color(0xFF3F0361),
              const Color(0xFF711D57),
            ],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(left:20.0,right: 20,bottom: 10),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
//                border: new Border.all(color: Color(0xFF4F4F4F),),
                color: Colors.white
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: ListView.builder(
                itemCount: 5,
                itemBuilder: (context, index) {
                  return myJobCard(index : index);
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
