import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:work4c/filter/jobDetails.dart';
import 'package:work4c/home/CheckIn.dart';

import '../test.dart';

class myJobCard extends StatefulWidget {
  final int index;
  myJobCard ({Key key,this.index}) : super(key: key);

  @override
  _myJobCardState createState() => _myJobCardState();
}

class _myJobCardState extends State<myJobCard> {

  TextEditingController controller = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return  Column(
      children: <Widget>[

        GestureDetector(
          onTap: (){
            Navigator.push(context,
                MaterialPageRoute(builder: (BuildContext context) => jobdetails()
                )
            );
          },
          child: new Slidable(
            key: ValueKey(widget.index),
            actionPane: SlidableDrawerActionPane(),
            dismissal: SlidableDismissal(
              child: SlidableDrawerDismissal(),
              onWillDismiss: (actionType) {
                return showDialog<bool>(
                  context: context,
                  builder: (context) {
                    return confirm();
                  },
                );
              },
            ),
            secondaryActions: <Widget>[
//            Container(
//              color: Color(0xFF19ACCA),
//              child: Column(
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                children: <Widget>[
//                  TextField(
//                    controller: controller,
////                    autofocus: true,
//                    decoration: InputDecoration(
//                        hintText: " Enter reason here why you want cancel",
//                        hintStyle: TextStyle(fontSize: 8,fontWeight: FontWeight.w300, color: Colors.white70)
//                    ),
//                  ),
//                  RaisedButton(
//                    shape: new RoundedRectangleBorder(
//                        borderRadius: new BorderRadius.circular(10.0)),
//                    color: Colors.white,
//                    onPressed: (){
//
//                    },
//                    child:  Text("ok",style: TextStyle(color: Colors.blue),),
//                  ),
//                ],
//              ),
//            ),
              IconSlideAction(
                  caption: 'Check-Out',
                  color: Color(0xFF19ACCA),
                  icon: Icons.cancel,
                onTap: ()=> confirm1(),
              ),


            ],
//          dismissal: SlidableDismissal(
//            child: SlidableDrawerDismissal(),
//          ),
            child: Container(
              color: Color(0xFF4F4F4F),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[

                  Padding(
                    padding: const EdgeInsets.only(left:8.0),
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Text("Work in site",style: new TextStyle(fontSize: 16.0, color: Colors.white,fontWeight: FontWeight.bold),),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Text(" CSGNY IT Consuluting Pvt Ltd.",style: new TextStyle(fontSize: 14.0, color: Colors.white),),
                          ),

                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  child: Column(
                                    children: <Widget>[
                                      Text("Hourly rate",style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Colors.white,
                                      ),),
                                      Padding(
                                        padding: const EdgeInsets.only(top:6.0,bottom: 6),
                                        child: Text("\$10 / \$14",style: new TextStyle(
                                            fontSize: 18.0,
                                            color: Colors.white ,fontWeight: FontWeight.bold
                                        ),),
                                      ),
                                    ],
                                  ),
                                ),

                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                  child: Container(
                                    height: 50,width: 1,
                                    color: Colors.white,
                                  ),
                                ),

                                Container(
                                  child: Column(
                                    children: <Widget>[
                                      Text("JOb Type",style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Colors.white,
                                      ),),
                                      Padding(
                                        padding: const EdgeInsets.only(top:6.0,bottom: 6),
                                        child: Text("Construction",style: new TextStyle(
                                            fontSize: 18.0,
                                            color: Colors.white ,fontWeight: FontWeight.bold
                                        ),),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.only(top:8.0),
                            child: Text("06 July 2017 - 23 July 2017",style: new TextStyle(fontSize: 14.0, color: Colors.white),),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Text(" 08:00 a.m. - 6:00 p.m.",style: new TextStyle(fontSize: 14.0, color: Colors.white),),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      GestureDetector(
                        child: Container(
                          height: 100,
                          width: 100,
                          decoration: new BoxDecoration(
                            boxShadow: [BoxShadow(
                              color: Colors.red,
                              blurRadius: 5.0,
                            ),],
                            color: Colors.black,
                            shape: BoxShape.circle,
                            border: new Border.all(
                              color: Color(0xFF19ACCA),
                              width: 4,
                            ),
                          ),
                          child: new Center(
                            child:Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.location_on,color: Colors.white,),
                                Text("Check-In",style: new TextStyle(fontSize: 14.0, color: Colors.white,fontWeight: FontWeight.bold),),
                              ],
                            ),

                          ),
                        ),
                        onTap: (){
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext   context) => new checkIn(),
                            ),
                          );
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:5.0),
                        child: Container(height: 150,width: 12,
                        color: Color(0xFF19ACCA),
                        child: Icon(Icons.arrow_back_ios,color: Colors.white,size: 20,),),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          height: 10,
          child: Text(""),
        ),
        Container(
          child: new Slidable(
            key: ValueKey(widget.index),
            actionPane: SlidableDrawerActionPane(),
            dismissal: SlidableDismissal(
              child: SlidableDrawerDismissal(),
              onWillDismiss: (actionType) {
                return showDialog<bool>(
                  context: context,
                  builder: (context) {
                    return confirm();
                  },
                );
              },
            ),
            secondaryActions: <Widget>[
              IconSlideAction(
                caption: 'Check-Out',
                color: Color(0xFF19ACCA),
                icon: Icons.cancel,
                onTap: ()=> confirm1(),
              ),
//              Container(
//                color: Color(0xFF19ACCA),
//                child: Column(
//                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                  children: <Widget>[
//                    TextField(
//                      controller: controller,
////                      autofocus: true,
//                      decoration: InputDecoration(
//                          hintText: " Enter reason here why you want cancel",
//                          hintStyle: TextStyle(fontSize: 8,fontWeight: FontWeight.w300, color: Colors.white70)
//                      ),
//                    ),
//                    RaisedButton(
//                      shape: new RoundedRectangleBorder(
//                          borderRadius: new BorderRadius.circular(10.0)),
//                      color: Colors.white,
//                      onPressed: (){
//                        SlidableDrawerDismissal();
//                      },
//                      child:  Text("ok",style: TextStyle(color: Colors.blue),),
//                    ),
//                  ],
//                ),
//              ),
//              IconSlideAction(
//                caption: 'Reject',
//                color: Color(0xFF19ACCA),
//                icon: Icons.cancel,
//              ),

            ],
//            dismissal: SlidableDismissal(
//              child: SlidableDrawerDismissal(),
//            ),
            child: Container(
              color: Color(0xFF4F4F4F),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[

                  Padding(
                    padding: const EdgeInsets.only(left:8.0),
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Text("Work in site",style: new TextStyle(fontSize: 16.0, color: Colors.white,fontWeight: FontWeight.bold),),
                          Padding(
                            padding:const EdgeInsets.symmetric(vertical: 8),
                            child: Text(" CSGNY IT Consuluting Pvt Ltd.",style: new TextStyle(fontSize: 14.0, color: Colors.white),),
                          ),

                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  child: Column(
                                    children: <Widget>[
                                      Text("Hourly rate",style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Colors.white,
                                      ),),
                                      Padding(
                                        padding: const EdgeInsets.only(top:6.0,bottom: 6),
                                        child: Text("\$10 / \$14",style: new TextStyle(
                                            fontSize: 18.0,
                                            color: Colors.white ,fontWeight: FontWeight.bold
                                        ),),
                                      ),
                                    ],
                                  ),
                                ),

                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                  child: Container(
                                    height: 50,width: 1,
                                    color: Colors.white,
                                  ),
                                ),

                                Container(
                                  child: Column(
                                    children: <Widget>[
                                      Text("JOb Type",style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Colors.white,
                                      ),),
                                      Padding(
                                        padding: const EdgeInsets.only(top:6.0,bottom: 6),
                                        child: Text("Construction",style: new TextStyle(
                                            fontSize: 18.0,
                                            color: Colors.white ,fontWeight: FontWeight.bold
                                        ),),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.only(top:8.0),
                            child: Text("06 July 2017 - 23 July 2017",style: new TextStyle(fontSize: 14.0, color: Colors.white),),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Text(" 08:00 a.m. - 6:00 p.m.",style: new TextStyle(fontSize: 14.0, color: Colors.white),),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        height: 100,
                        width: 100,
                        decoration: new BoxDecoration(
                          color: Colors.black,
                          shape: BoxShape.circle,
                          border: new Border.all(
                            color: Color(0xFF19ACCA),
                            width: 4,
                          ),
                        ),
                        child: new Center(
                          child:Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("02",style: new TextStyle(fontSize: 14.0, color: Color(0xFF19ACCA),fontWeight: FontWeight.bold),),
                                    Text("09",style: new TextStyle(fontSize: 14.0, color: Color(0xFF19ACCA),fontWeight: FontWeight.bold),),
                                    Text("56",style: new TextStyle(fontSize: 14.0, color: Color(0xFF19ACCA),fontWeight: FontWeight.bold),),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("Days",style: new TextStyle(fontSize: 10.0, color: Colors.white,fontWeight: FontWeight.bold),),
                                    Text("Hrs",style: new TextStyle(fontSize: 10.0, color: Colors.white,fontWeight: FontWeight.bold),),
                                    Text("Min",style: new TextStyle(fontSize: 10.0, color: Colors.white,fontWeight: FontWeight.bold),),
                                  ],
                                ),
                                Text("Till Start",style: new TextStyle(fontSize: 14.0, color: Colors.grey,fontWeight: FontWeight.bold),),

                              ],
                            ),
                          ),

                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:5.0),
                        child: Container(height: 150,width: 12,
                        color: Color(0xFF19ACCA),
                        child: Icon(Icons.arrow_back_ios,color: Colors.white,size: 20,),),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          height: 10,
          child: Text(""),
        ),

      ],
    );
  }

  void confirm1() {

    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text('Check Out Reason'),
          content: TextField(
            controller: controller,
//                      autofocus: true,
            decoration: InputDecoration(
                hintText: " Enter reason here why you want cancel",
                hintStyle: TextStyle(fontSize: 12,fontWeight: FontWeight.w300, color: Colors.black)
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () => Navigator.of(context).pop(false),
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () { Navigator.of(context).pop(false);
              setState(() {
              });},
            ),
          ],
        );
      },
    );
  }

  Widget confirm() {
    return( AlertDialog(
      title: Text('Check Out Reason'),
      content: TextField(
        controller: controller,
//                      autofocus: true,
        decoration: InputDecoration(
            hintText: " Enter reason here why you want cancel",
            hintStyle: TextStyle(fontSize: 12,fontWeight: FontWeight.w300, color: Colors.black)
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Cancel'),
          onPressed: () => Navigator.of(context).pop(false),
        ),
        FlatButton(
          child: Text('Ok'),
          onPressed: () { Navigator.of(context).pop(false);
          setState(() {
          });},
        ),
      ],
    ));
  }
}
